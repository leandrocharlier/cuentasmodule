package com.example.cuentasmodulee;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class CuentasFragment extends Fragment {

    private CuentasNavigation cuentasNavigation;

    public CuentasFragment(CuentasNavigation cuentasNavigation) {
        this.cuentasNavigation = cuentasNavigation;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cuentas, container, false);

        Button button = view.findViewById(R.id.button_tarjetas);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cuentasNavigation.goToTarjetas(getActivity(), "1234");
            }
        });
        return view;
    }

}
