package com.example.cuentasmodulee;

import android.app.Activity;

public interface CuentasNavigation {
    void goToTarjetas(Activity activity, String cuenta);
}
