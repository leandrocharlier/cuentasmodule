package com.example.cuentasmodule;

import android.app.Activity;
import android.app.AlertDialog;

import com.example.cuentasmodulee.CuentasNavigation;

public class StubCuentasNavigation implements CuentasNavigation {

    @Override
    public void goToTarjetas(Activity activity, String cuenta) {
        new AlertDialog.Builder(activity).setMessage("Not implemented!").show();
    }
}
