package com.example.cuentasmodule;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.cuentasmodulee.CuentasFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CuentasFragment cuentasFragment = new CuentasFragment(new StubCuentasNavigation());

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, cuentasFragment)
                .commit();
    }
}
